CREATE TABLE IF NOT EXISTS `order_detail` (
    `id` BIGINT AUTO_INCREMENT,
    `amount` integer not null,
    `product_id` bigint,
    primary key (`id`),
    foreign key (`product_id`) references product(`id`)
) DEFAULT CHARSET=utf8;