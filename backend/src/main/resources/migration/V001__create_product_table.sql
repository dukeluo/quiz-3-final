CREATE TABLE IF NOT EXISTS product (
    `id`            BIGINT              AUTO_INCREMENT PRIMARY KEY,
    `name`    VARCHAR(64)         NOT NULL,
    `price`     INT        NOT NULL,
    `unit`     VARCHAR(64)         NOT NULL,
    `picture_url`     VARCHAR(255)         NOT NULL
) DEFAULT CHARSET=utf8;

INSERT INTO product (name, price, unit, picture_url) VALUES
  ('apple', 6, '个', 'https://www.kimmelorchard.org/sites/default/files/fruit/kimmel-apple-jonathan.jpg'),
  ('orange', 8, '个', 'https://i.pinimg.com/originals/63/72/9f/63729fee451eeedea622815b2a2e3e76.png');
