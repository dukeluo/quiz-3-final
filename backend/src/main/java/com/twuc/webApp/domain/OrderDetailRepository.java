package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {
    Optional<List<OrderDetail>> findAllByOrderByIdAsc();
    Optional<OrderDetail> findOrderDetailByProductId(Long productId);
}
