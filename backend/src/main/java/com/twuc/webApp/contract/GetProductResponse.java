package com.twuc.webApp.contract;

import com.twuc.webApp.domain.Product;

import java.util.Objects;

public class GetProductResponse {

    private Long id;
    private String name;
    private Integer price;
    private String unit;
    private String pictureUrl;

    public GetProductResponse(Product product) {
        Objects.requireNonNull(product);

        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.unit = product.getUnit();
        this.pictureUrl = product.getPictureUrl();
    }

    public GetProductResponse() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }
}
