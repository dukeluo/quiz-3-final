package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;

public class DeleteOrderRequest {
    @NotNull
    private Long productId;

    public DeleteOrderRequest(@NotNull Long productId) {
        this.productId = productId;
    }

    public DeleteOrderRequest() {
    }

    public Long getProductId() {
        return productId;
    }
}
