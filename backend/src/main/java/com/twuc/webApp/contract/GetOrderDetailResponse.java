package com.twuc.webApp.contract;

import com.twuc.webApp.domain.OrderDetail;

import java.util.Objects;

public class GetOrderDetailResponse {

    private Long productId;
    private String name;
    private Integer price;
    private String unit;
    private Integer amount;

    public GetOrderDetailResponse(OrderDetail orderDetail) {
        Objects.requireNonNull(orderDetail);

        this.productId = orderDetail.getProduct().getId();
        this.name = orderDetail.getProduct().getName();
        this.price = orderDetail.getProduct().getPrice();
        this.unit = orderDetail.getProduct().getUnit();
        this.amount = orderDetail.getAmount();
    }

    public GetOrderDetailResponse() {
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public Integer getAmount() {
        return amount;
    }

    public Long getProductId() {
        return productId;
    }
}
