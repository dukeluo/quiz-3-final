package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;

public class AddProductToOrderRequest {
    @NotNull
    private Long productId;

    @NotNull
    private Integer amount;

    public AddProductToOrderRequest(@NotNull Long productId, @NotNull Integer amount) {
        this.productId = productId;
        this.amount = amount;
    }

    public AddProductToOrderRequest() {
    }

    public Long getProductId() {
        return productId;
    }

    public Integer getAmount() {
        return amount;
    }
}
