package com.twuc.webApp.contract;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExceptionResponse {
    private Exception exception;

    public ExceptionResponse(Exception exception) {
        this.exception = exception;
    }

    @JsonProperty(value = "message")
    public String getExceptionMessage() {
        return exception.getMessage();
    }
}
