package com.twuc.webApp.contract;

import com.twuc.webApp.domain.Product;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AddProductRequest {
    @NotNull
    @Size(min = 1, max = 64)
    private String name;

    @NotNull
    private Integer price;

    @NotNull
    @Size(min = 1, max = 64)
    private String unit;

    @NotNull
    @Size(min = 1, max = 255)
    private String pictureUrl;

    public AddProductRequest() {
    }

    public AddProductRequest(@NotNull @Size(min = 1, max = 64) String name,
                             @NotNull Integer price,
                             @NotNull @Size(min = 1, max = 64) String unit,
                             @NotNull @Size(min = 1, max = 255) String pictureUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.pictureUrl = pictureUrl;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public Product toProduct() {
        return new Product(name, price, unit, pictureUrl);
    }
}
