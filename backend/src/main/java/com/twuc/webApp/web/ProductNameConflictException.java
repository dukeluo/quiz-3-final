package com.twuc.webApp.web;

public class ProductNameConflictException extends RuntimeException {

    @Override
    public String getMessage() {
        return "商品名称已存在，请输入新的商品名称";
    }
}
