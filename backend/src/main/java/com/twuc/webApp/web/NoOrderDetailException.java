package com.twuc.webApp.web;

public class NoOrderDetailException extends RuntimeException {

    @Override
    public String getMessage() {
        return "暂无订单，返回商城页面继续购买";
    }
}
