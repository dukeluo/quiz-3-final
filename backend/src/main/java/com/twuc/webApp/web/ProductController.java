package com.twuc.webApp.web;

import com.twuc.webApp.contract.*;
import com.twuc.webApp.domain.*;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*", exposedHeaders = {HttpHeaders.LOCATION})
@RequestMapping(value = "/api")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @GetMapping(value = "/products")
    public ResponseEntity<List<GetProductResponse>> getProducts() {
        List<Product> products = productRepository.findAllByOrderByIdAsc().orElse(new ArrayList<>());
        List<GetProductResponse> getProductResponses = products.stream()
                .map(GetProductResponse::new)
                .collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(getProductResponses);
    }

    @PostMapping(value = "/product")
    public ResponseEntity<Void> addProduct(@Valid @RequestBody AddProductRequest addProductRequest) {
        List<Product> allProducts = productRepository.findAll();
        List<String> allProductName = allProducts.stream().map(Product::getName).collect(Collectors.toList());
        Product product = addProductRequest.toProduct();

        if (allProductName.contains(product.getName())) {
            throw new ProductNameConflictException();
        }
        productRepository.save(product);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(null);
    }

    @GetMapping(value = "/orders")
    public ResponseEntity<List<GetOrderDetailResponse>> getOrder() {
        List<OrderDetail> orderDetails = orderDetailRepository.findAllByOrderByIdAsc().orElse(new ArrayList<>());

        if (orderDetails.size() == 0) {
            throw new NoOrderDetailException();
        }
        List<GetOrderDetailResponse> getProductResponses = orderDetails.stream()
                .map(GetOrderDetailResponse::new)
                .collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(getProductResponses);
    }

    @PatchMapping(value = "/orders")
    public ResponseEntity<Void> addProductToOrder(@Valid @RequestBody AddProductToOrderRequest addProductToOrderRequest) {
        Integer amount = addProductToOrderRequest.getAmount();
        Long productId = addProductToOrderRequest.getProductId();
        Product product = productRepository.findById(productId).get();
        Optional<OrderDetail> orderDetailFound = orderDetailRepository.findOrderDetailByProductId(productId);
        OrderDetail orderDetail;

        if (orderDetailFound.isPresent()) {
            orderDetail = orderDetailFound.get();
            orderDetail.setAmount(orderDetail.getAmount()+amount);
        } else {
            orderDetail = new OrderDetail(product, amount);
        }
        orderDetailRepository.save(orderDetail);
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(null);
    }

    @DeleteMapping(value = "/orders")
    public ResponseEntity<Void> deleteOrder(@Valid @RequestBody DeleteOrderRequest deleteOrderRequest) {
        Long productId = deleteOrderRequest.getProductId();
        Product product = productRepository.findById(productId).get();
        OrderDetail orderDetailFound = orderDetailRepository.findOrderDetailByProductId(productId).get();

        product.setOrderDetail(null);
        orderDetailRepository.deleteById(orderDetailFound.getId());
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(null);
    }

    @ExceptionHandler(value = {ProductNameConflictException.class})
    public ResponseEntity<ExceptionResponse> runTimeExceptionHandler(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ExceptionResponse(exception));
    }

    @ExceptionHandler(value = {NoOrderDetailException.class})
    public ResponseEntity<ExceptionResponse> NoOrderDetailExceptionHandler(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ExceptionResponse(exception));
    }

}
