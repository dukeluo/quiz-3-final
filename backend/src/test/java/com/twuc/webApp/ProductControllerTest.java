package com.twuc.webApp;

import com.twuc.webApp.domain.OrderDetail;
import com.twuc.webApp.domain.OrderDetailRepository;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

class ProductControllerTest extends ApiTestBase {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Test
    void should_get_all_products() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/products"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].unit").value("个"))
                .andExpect(jsonPath("$[0].name").value("apple"))
                .andExpect(jsonPath("$[0].price").value(6))
                .andExpect(jsonPath("$[0].pictureUrl").value("https://www.kimmelorchard.org/sites/default/files/fruit/kimmel-apple-jonathan.jpg"))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].unit").value("个"))
                .andExpect(jsonPath("$[1].name").value("orange"))
                .andExpect(jsonPath("$[1].pictureUrl").value("https://i.pinimg.com/originals/63/72/9f/63729fee451eeedea622815b2a2e3e76.png"))
                .andExpect(jsonPath("$[1].price").value(8));
    }

    @Test
    void should_add_a_product() throws Exception {
        String content = "{\n" +
                "   \"name\":\"pear\",\n" +
                "   \"price\":10,\n" +
                "   \"unit\":\"个\",\n" +
                "   \"pictureUrl\":\"https://img30.360buyimg.com/cf/s128x128_jfs/t2080/5/1661817558/173133/51d26439/566e948eN9d443d24.jpg\"\n" +
                "}";

        mockMvc.perform(MockMvcRequestBuilders.post("/api/product")
                    .content(content)
                    .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().is(200));
        assertTrue(productRepository.findProductByName("pear").isPresent());
    }

    @Test
    void should_get_status_400_when_add_a_product_which_name_is_duplicate() throws Exception {
        String content = "{\n" +
                "   \"name\":\"apple\",\n" +
                "   \"price\":10,\n" +
                "   \"unit\":\"个\",\n" +
                "   \"pictureUrl\":\"https://img30.360buyimg.com/cf/s128x128_jfs/t2080/5/1661817558/173133/51d26439/566e948eN9d443d24.jpg\"\n" +
                "}";

        mockMvc.perform(MockMvcRequestBuilders.post("/api/product")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(jsonPath("$.message").value("商品名称已存在，请输入新的商品名称"));
    }

    @Test
    void should_get_status_404_when_no_order_detail() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/orders"))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(jsonPath("$.message").value("暂无订单，返回商城页面继续购买"));
    }

    @Test
    void should_get_all_orders() throws Exception {
        Product apple = productRepository.findProductByName("apple").get();
        Product orange = productRepository.findProductByName("orange").get();

        OrderDetail orderDetail = new OrderDetail(apple, 2);
        OrderDetail orderDetailAnother = new OrderDetail(orange, 4);

        orderDetailRepository.save(orderDetail);
        orderDetailRepository.save(orderDetailAnother);
        orderDetailRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.get("/api/orders"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$[0].name").value(apple.getName()))
                .andExpect(jsonPath("$[0].price").value(apple.getPrice()))
                .andExpect(jsonPath("$[0].amount").value(orderDetail.getAmount()))
                .andExpect(jsonPath("$[1].name").value(orange.getName()))
                .andExpect(jsonPath("$[1].price").value(orange.getPrice()))
                .andExpect(jsonPath("$[1].amount").value(orderDetailAnother.getAmount()));
    }

    @Test
    void should_add_product_to_order() throws Exception {
        String content = "{\"productId\": 1, \"amount\": 2}";

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/orders")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(content))
                .andExpect(MockMvcResultMatchers.status().is(201));
        assertTrue(orderDetailRepository.findOrderDetailByProductId(1L).isPresent());

        OrderDetail orderDetail = orderDetailRepository.findOrderDetailByProductId(1L).get();
        assertEquals(2, orderDetail.getAmount().intValue());
    }

    @Test
    void should_add_product_to_the_existed_order() throws Exception {
        String content = "{\"productId\": 1, \"amount\": 2}";
        Product apple = productRepository.findProductByName("apple").get();

        OrderDetail orderDetail = new OrderDetail(apple, 2);

        orderDetailRepository.save(orderDetail);
        orderDetailRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(content))
                .andExpect(MockMvcResultMatchers.status().is(201));
        assertTrue(orderDetailRepository.findOrderDetailByProductId(1L).isPresent());

        OrderDetail orderDetailFound = orderDetailRepository.findOrderDetailByProductId(1L).get();
        assertEquals(4, orderDetailFound.getAmount().intValue());
    }

    @Test
    void should_delete_the_existed_order() throws Exception {
        String content = "{\"productId\": 1}";
        Product apple = productRepository.findProductByName("apple").get();
        OrderDetail orderDetail = new OrderDetail(apple, 2);

        orderDetailRepository.save(orderDetail);
        orderDetailRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(content))
                .andExpect(MockMvcResultMatchers.status().is(200));
        assertFalse(orderDetailRepository.findOrderDetailByProductId(1L).isPresent());
    }

}