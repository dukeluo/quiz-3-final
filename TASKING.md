### 配置环境，准备初始数据

初始数据有两条，如下：

`{"id":1,"name":"apple","price":6,"unit":"个","pictureUrl":"https://www.kimmelorchard.org/sites/default/files/fruit/kimmel-apple-jonathan.jpg"}`和`{"id":2,"name":"orange","price":8,"unit":"个","pictureUrl":"https://i.pinimg.com/originals/63/72/9f/63729fee451eeedea622815b2a2e3e76.png"}`

### 后端

#### 获取所有商品
- 当使用get方法对访问api`/api/products`时，应该得到状态码200，并且content内容为包含商品信息的数组，包括名称、价格、单位、商品图片链接信息

#### 获取所有订单
- 给定未添加商品至订单的情况，当使用get方法对api`/api/orders`访问时，应该得到状态码404，并且content内容为`暂无订单，返回商城页面继续购买`
- 给定添加商品至订单的情况，当使用get方法对api`/api/orders`访问时，应该得到状态码200，并且content内容为包含订单信息的数组，包括商品，以及数量信息

#### 添加商品至订单
- 当使用patch方法对api`/api/orders`访问时，其content内容为`{"productId": 1, "amount": 2}`，应该成功添加订单信息，得到状态码201

#### 添加商品
- 当使用post方法对api`/api/product`访问时，其content内容为`{"name":"pear","price":10,"unit":"个","pictureUrl":"https://img30.360buyimg.com/cf/s128x128_jfs/t2080/5/1661817558/173133/51d26439/566e948eN9d443d24.jpg"}`，应该成功添加商品，得到状态码200
- 当使用post方法对api`/api/product`访问时，其content内容为`{"name":"apple","price":10,"unit":"个","pictureUrl":"https://img30.360buyimg.com/cf/s128x128_jfs/t2080/5/1661817558/173133/51d26439/566e948eN9d443d24.jpg"}`，不能成功添加商品，得到状态码400，并且content内容为`商品名称已存在，请输入新的商品名称`

#### 删除订单
- 当使用delete方法对api`/api/orders`访问时，其content内容为`{"productId": 1}`，应该成功删除订单信息，得到状态码200

### 前端

#### 添加路由

#### 添加商店头部

#### 添加商店底部

#### 添加商品展示页

#### 添加添加商品页

#### 添加添加订单页



#### 





