const getAllProducts = () => (dispatch) => {
    let URL = 'http://localhost:8080/api/products';
    let info = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }    
    };

    fetch(URL, info)
        .then(response => response.json())
        .then(result => {
            dispatch({
                type: 'GET_ALL_PRODUCTS',
                products: result,
            });
    }).catch(error => {
        console.error(error);
    });
};

const addProduct = (newProduct) => (dispatch) => {
    let URL = 'http://localhost:8080/api/product';
    let info = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        },
        body: JSON.stringify(newProduct),
    };

    fetch(URL, info)
        .then(response => {
            if (response.status === 200) {
                dispatch({
                    type: 'ADD_PRODUCT',
                    isAddProductSuccessful: true,
                    message: '',
                });
            }
            if (response.status === 400) {

                response.json().then((result) => {
                    dispatch({
                        type: 'ADD_PRODUCT',
                        isAddProductSuccessful: false,
                        message: result.message,
                    });
                });
            }
        })
        .catch(error => {
            console.error(error);
        });
};

const getAllOrders = () => (dispatch) => {
    let URL = 'http://localhost:8080//api/orders';
    let info = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }    
    };

    fetch(URL, info)
        .then(response => {
            if (response.status === 404) {
                response.json().then((result) => {
                    dispatch({
                        type: 'GET_ALL_ORDERS',
                        isOrderExisted: false,
                        message: result.message,
                        orders: [],
                    });
                });
            }
            if (response.status === 200) {
                response.json().then((result) => {
                    dispatch({
                        type: 'GET_ALL_ORDERS',
                        isOrderExisted: true,
                        message: '',
                        orders: result,
                    });
                });
            }
        })
        .catch(error => {
            console.error(error);
        });
};

const addProductToOrder = (newOrder) => (dispatch) => {
    let URL = 'http://localhost:8080/api/orders';
    let info = {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        },
        body: JSON.stringify(newOrder),
    };

    fetch(URL, info)
        .then(response => {
            if (response.status === 201) {
                dispatch({
                    type: 'ADD_PRODUCT_TO_ORDER',
                });
            }
        })
        .catch(error => {
            console.error(error);
        });

};

const deleteOrder = (newDelete, callback) => (dispatch) => {
    let URL = 'http://localhost:8080/api/orders';
    let info = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        },
        body: JSON.stringify(newDelete),
    };

    fetch(URL, info)
        .then(response => {
            if (response.status === 200) {
                dispatch({
                    type: 'DELETE_ORDER',
                });
                callback();
            }
        })
        .catch(error => {
            console.error(error);
        });
};

export {
    getAllProducts,
    addProduct,
    getAllOrders,
    addProductToOrder,
    deleteOrder,
};