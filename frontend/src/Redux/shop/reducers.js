const initState = {
    products: [],
    orders:[],
    message: '',
    isOrderExisted: true,
};
  
export default (state = initState, action) => {    
    switch (action.type) {
        case 'GET_ALL_PRODUCTS':
            return {
                ...state,
                products: action.products
            };
        case 'ADD_PRODUCT':
            return {
                ...state,
                isAddProductSuccessful: action.isAddProductSuccessful,
                message: action.message,
            };
        case 'GET_ALL_ORDERS':
            return {
                ...state,
                isOrderExisted: action.isOrderExisted,
                message: action.message,
                orders: action.orders,
            };
        default:
            return state;
    }
};
