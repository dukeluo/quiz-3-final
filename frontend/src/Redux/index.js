import {combineReducers} from "redux";
import shopReducer from './shop/reducers';


const reducers = combineReducers({
    shop: shopReducer,
});
export default reducers;