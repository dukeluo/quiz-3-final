import React, { Component } from 'react';
import ShopHeader from '../../Components/ShopHeader/ShopHeader';
import ProductList from '../../Components/ProductList/ProductList';
import ShopBottom from '../../Components/ShopBottom/ShopBottom';
import './Home.less';

class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div className='home'>
                <ShopHeader />
                <ProductList />
                <ShopBottom />
            </div>
        );
    }
}

export default Home;