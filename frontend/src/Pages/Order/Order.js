import React, { Component } from 'react';
import './Order.less';
import ShopHeader from '../../Components/ShopHeader/ShopHeader';
import ShopBottom from '../../Components/ShopBottom/ShopBottom';
import OrderList from '../../Components/OrderList/OrderList';

class Order extends Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div className='order'>
                <ShopHeader />
                <OrderList />
                <ShopBottom />
            </div>
        );
    }
}

export default Order;
