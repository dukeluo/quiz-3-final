import React, { Component } from 'react';
import './AddProduct.less';
import ShopHeader from '../../Components/ShopHeader/ShopHeader';
import ShopBottom from '../../Components/ShopBottom/ShopBottom';
import NewProduct from '../../Components/NewProduct/NewProduct';

export class AddProduct extends Component {
    render() {
        return (
            <div className="add-product">
                <ShopHeader />
                <NewProduct />
                <ShopBottom />
            </div>
        );
    }
}

export default AddProduct;
