import React, {Component} from 'react';
import './App.less';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from "../Pages/Home/Home";
import Order from '../Pages/Order/Order';
import AddProduct from '../Pages/AddProduct/AddProduct';

class App extends Component{
  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Route path="/product" component={AddProduct}/>
            <Route path="/orders" component={Order}/>
            <Route path="/" component={Home}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;