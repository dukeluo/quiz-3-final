import React from 'react';
import './ShopHeader.less';
import {Link} from 'react-router-dom';
import { MdHome } from "react-icons/md";
import { MdAddShoppingCart } from "react-icons/md";
import { MdAdd } from "react-icons/md";

const ShopHeader = () => {
    return (
        <header className='shop-header'>
            <ul>
                <li>
                    <div className='shop-header-item'>
                        <MdHome />
                        <Link to="/">商城</Link>
                    </div>
                </li>
                <li>
                    <div className='shop-header-item'>
                        <MdAddShoppingCart />
                        <Link to="/orders">订单</Link>
                    </div>
                </li>
                <li>
                    <div className='shop-header-item'>
                        <MdAdd />
                        <Link to="/product">添加商品</Link>
                    </div>
                </li>
            </ul>
        </header>
    );
}

export default ShopHeader;