import React, { Component } from 'react';
import {connect} from "react-redux";
import {getAllProducts} from '../../Redux/shop/actions';
import {bindActionCreators} from "redux";
import './ProductList.less';
import { MdAdd } from "react-icons/md";
import {addProductToOrder} from '../../Redux/shop/actions';

class ProductList extends Component {
    constructor(props) {
        super(props);
        this.onClickHandler = this.onClickHandler.bind(this);
    }

    onClickHandler(index) {
        let productId = this.props.products[index].id;
        let newOrder = {
            "amount": 1,
            productId,
        }

        this.props.addProductToOrder(newOrder);
    }

    render() {
        const products = this.props.products;
        const productItems = products.map((item, index) => (
                <li className='list-item' key={index}>
                    <img src={item.pictureUrl} alt={item.name}/>
                    <p>{item.name}</p>
                    <p>{`单价: ${item.price}元/${item.unit}`}</p>
                    <MdAdd className='add-btn' onClick={() => this.onClickHandler(index)}/>
                </li>
            )
        );

        return (
            <div className='product-list'>
                <ul>
                    {productItems}
                </ul>
            </div>
        );
    }

    componentDidMount() {
        this.props.getAllProducts();
    }
}

const mapStateToProps = state => ({
    products: state.shop.products
});
  
const mapDispatchToProps = dispatch => bindActionCreators({
    getAllProducts, 
    addProductToOrder,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);