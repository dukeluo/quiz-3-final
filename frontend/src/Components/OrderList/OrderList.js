import React, { Component } from 'react';
import './OrderList.less';
import {getAllOrders} from '../../Redux/shop/actions';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Button from '../Button/Button';
import {deleteOrder} from '../../Redux/shop/actions';

class OrderList extends Component {
    constructor(props, context) {
        super(props, context);
        this.renderTableData = this.renderTableData.bind(this);
        this.onClickHandler = this.onClickHandler.bind(this);
    }

    onClickHandler(index) {
        let order = this.props.orders[index];
        let { productId } = order;

        this.props.deleteOrder({
            productId,
        }, this.props.getAllOrders);
    }

    renderTableData(orders) {
        return orders.map((order, index) => {
           const { name, price, unit, amount } = order;

           return (
              <tr className='order-item' key={index}>
                 <td>{name}</td>
                 <td>{price}</td>
                 <td>{amount}</td>
                 <td>{unit}</td>
                 <td><Button text='删除' handler={() => this.onClickHandler(index)}/></td>
              </tr>
           );
        });
     }

    render() {
        const orders = this.props.orders;
        
        return (
            <div className='order-list'>
                <table>
                    <thead>
                        <tr>
                            <th>名字</th>
                            <th>单价</th>
                            <th>数量</th>
                            <th>单位</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTableData(orders)}
                    </tbody>
                </table>
            </div>
        );
    }

    componentDidMount() {
        this.props.getAllOrders();
    }

    componentDidUpdate(prevProps) {
        if (!this.props.isOrderExisted && this.props !== prevProps) {
            alert(this.props.message);
        }
    }
}

const mapStateToProps = state => ({
    orders: state.shop.orders,
    isOrderExisted: state.shop.isOrderExisted,
    message: state.shop.message,
});
  
const mapDispatchToProps = dispatch => bindActionCreators({
    getAllOrders,
    deleteOrder,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);