import React, { Component } from 'react';
import './NewProduct.less';
import Button from '../Button/Button';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {addProduct} from '../../Redux/shop/actions';

class NewProduct extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            name: '',
            price: '',
            unit: '',
            url: '',
            isPriceNumber: false,
        }
        this.inputChangeHandler = this.inputChangeHandler.bind(this);
        this.buttonClickHandler = this.buttonClickHandler.bind(this);
    }

    inputChangeHandler(event) {
        let i = 0;
        
        for (let sibling = event.target.parentNode.previousSibling; sibling !== null; sibling = sibling.previousSibling) {
            i++;
        };
        
        const keys = Object.keys(this.state);
        
        this.setState({
            [keys[i]]: event.target.value,
        });
    }

    buttonClickHandler() {
        const isPriceNumber = /^\d+$/.test(this.state.price);

        this.setState({
            isPriceNumber,
        });
        
        if (isPriceNumber) {
            let newProduct = {
                "name": this.state.name,
                "price": +this.state.price,
                "unit": this.state.unit,
                "pictureUrl": this.state.url,
            };

            this.props.addProduct(newProduct);
        } else {
            console.error("The price is not a number!");
        }
    }

    render() {
        return (
            <div className='new-product'>
                <h2>添加商品</h2>
                <form>
                    <div className='product-input'>
                        <label htmlFor='product-name'><span>* </span>名称：</label>
                        <input type="text" id='product-name' name='product-name' placeholder='名称' onChange={this.inputChangeHandler} required/>
                    </div>
                    <div className='product-input'>
                        <label htmlFor='product-price'><span>* </span>价格：</label>
                        <input type="text" id='product-price' name='product-price' placeholder='价格' onChange={this.inputChangeHandler} required/>
                    </div>
                    <div className='product-input'>
                        <label htmlFor='product-unit'><span>* </span>单位：</label>
                        <input type="text" id='product-unit' name='product-unit' placeholder='单位' onChange={this.inputChangeHandler} required/>
                    </div>
                    <div className='product-input'>
                        <label htmlFor='product-picture'><span>* </span>图片：</label>
                        <input type="text" id='product-picture' name='product-picture' placeholder='URL' onChange={this.inputChangeHandler} required/>
                    </div>
                </form>
                <Button text='提交' handler={this.buttonClickHandler}/>
            </div>
        );
    }
    
    componentDidUpdate(prevProps) {
        if (this.props.isAddProductSuccessful === false && this.props !== prevProps) {
            alert(this.props.message);
        }
    }
}

const mapStateToProps = state => ({
    isAddProductSuccessful: state.shop.isAddProductSuccessful,
    message: state.shop.message,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    addProduct,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);